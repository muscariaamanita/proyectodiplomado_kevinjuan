<?php

class conexion{
    function recuperarDatos(){
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dataBase = "espaciosusbcali";

    $conn = new mysqli($servername, $username, $password, $dataBase);
    $conn->set_charset("utf8");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM espacios";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo "Id: " . $row["Id"]. " - Nombre: " . $row["Nombre"]. " - Latitud: " . $row["Latitud"]. 
             " - Longitud: " .$row["Longitud"]. " - Descripcion: " .$row["Descripcion"]. "- Visible: " .$row["Visible"].
             " - Pertenece A: " .$row["PerteneceA"]."<br>";
        }
    } else {
        echo "0 results";
    }
    $conn->close();
    }
}


?>