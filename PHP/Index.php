<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- Esto cambia el FavIcon-->
    <link rel="shortcut icon" type="image/png" href="http://www.pngall.com/wp-content/uploads/2017/05/Map-Marker-PNG-Picture.png"/>

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <title>Cartografica universidad de San Buenaventura</title>
</head>
<body>
    <div id="Content">

        <div id="contentBtn">

            <div id="Search" data-toggle="tooltip" data-placement="left" title="Buscar">
                <button class="btn btn-info btn-circle btn-lg" id="btnSearch" data-toggle="modal" data-target="#modalLupa" >
                <i class="fas fa-search"></i>
                </button>
            </div>

            <div id="Filter" data-toggle="tooltip" data-placement="left" title="Filtrar">
                <button class="btn btn-danger btn-circle btn-lg" id="btnFilter" data-toggle="modal" data-target="#myModal">
                <i class="fas fa-filter"></i>
                </button>
            </div>
         
        </div>

        <div id="contentInfo">

            <div id="Info" data-toggle="tooltip" data-placement="right" title="Ayuda">
                <button class="btn btn-warning btn-circle btn-lg" id="btnInfo" data-toggle="modal" data-target="#modalInfo"><i class="fas fa-info"></i></button>
            </div>

        </div>


        <div id="map">
            <!-- Aqui se carga el mapa-->
        </div>
        
    </div>
    

  <!-- ######################################################################## -->
  <!-- Aqui vamos a meter el ajax -->


  <!-- ######################################################################## -->
  <!-- Aqui esta el formulario de los filtros  -->
    <div class="container">
        
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filtros para la busqueda</h4>
              </div>
              <div class="modal-body">
                
                <!-- Aqui va el sexy formulario que segmenta los nodos-->

                <form action="procesar.php" method="post" name="form" id="formulario">
                <!-- primera parte del formulario -->
                <div class="tituloFiltros">El lugar es un:</div>
                <br>
                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Salon" name="Salon">
                <label class="form-check-label" for="inlineCheckbox1">Salon</label>
                </div>
                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="Laboratorio" name="Laboratorio">
                <label class="form-check-label" for="defaultCheck2">Laboratorio</label>
                </div>
                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="Auditorio" name="Auditorio">
                <label class="form-check-label" for="defaultCheck3">Auditorio</label>
                </div>
                <br>
                <!-- Segunda parte del formulario -->
                <div class="tituloFiltros">El lugar se encuentra en: </div>  
                <br>
                <div id="contenedorLugares">
                <div id="contenedorLugaresIzq">
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios1" value="Farallones" name="Farallones">
                <label class="form-check-label" for="defaultCheck1">Farallones</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios2" value="Palmas" name="Palmas">
                <label class="form-check-label" for="defaultCheck2">Palmas</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios3" value="Cerezos" name="Cerezos">
                <label class="form-check-label" for="defaultCheck3">Cerezos</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios4" value="Horizontes" name="Horizontes">
                <label class="form-check-label" for="defaultCheck4">Horizontes</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios5" value="Biblioteca" name="Biblioteca">
                <label class="form-check-label" for="defaultCheck5">Biblioteca</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios6" value="Lago" name="Lago">
                <label class="form-check-label" for="defaultCheck6">Lago</label>
                </div>
                </div>

                <div id="contenedorLugaresDer">
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios7" value="Cedro" name="Cedro">
                <label class="form-check-label" for="defaultCheck7">Cedro</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios8" value="Naranjos" name="Naranjos">
                <label class="form-check-label" for="defaultCheck8">Naranjos</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios9" value="Higuerones 1" name="Higuerones1">
                <label class="form-check-label" for="defaultCheck9">Higuerones 1</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios10" value="Higuerones 2" name="Higuerones2">
                <label class="form-check-label" for="defaultCheck10">Higuerones 2</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios11" value="Parque Tecnologico" name="ParqueTecnologico">
                <label class="form-check-label" for="defaultCheck11">Parque Tecnologico</label>
                </div>
                <div>
                <input class="form-check-input" type="radio" id="exampleRadios12" value="CDU" name="CDU">
                <label class="form-check-label" for="defaultCheck12">CDU</label>
                </div>
                </div>
                </div>

                <!-- Tercer parte formulario -->
                <br>
                <div class="tituloFiltros">Lo que quieres ver:</div>
                <br>
                <div>                
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="Fotos360" name="Fotos360">
                <label class="form-check-label" for="inlineCheckbox4">Fotos 360</label>
                </div>

                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="Videos" name="Videos">
                <label class="form-check-label" for="defaultCheck5">Videos</label>
                </div>

                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="Audios" name="Audios">
                <label class="form-check-label" for="defaultCheck6">Audios</label>
                </div>
                
                <div>
                <input class="form-check-input" type="checkbox" id="inlineCheckbox7" value="Modelo3D" name="Modelo3D">
                <label class="form-check-label" for="defaultCheck7">Modelo 3D</label>
                </div>
                <br>



                <button type="submit" class="btn btn-primary" value="enviar" id="enviar">Aplicar filtros</button>
                </form>

                <div id="resp"></div>

                <?php
                    include("Conexion.php");
                    $conn = new conexion();
                    $conn->recuperarDatos();
                ?>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            
          </div>
        </div>
        
      </div>
      
     <!-- ################################################################################# -->

     <!-- Aqui coge el modal de la informacion "About us"-->
      
     <div class="container">
        
        <!-- Modal -->
        <div class="modal fade" id="modalInfo" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div id="contenedorLogos">
                
                    <div id="logoSanbue">
                      <img src="../SRC/logo universidad.png" id="imagenLogoUsb" alt="Smiley face" height="130" width="130">
                    </div>

                    <div id="logoProyecto">
                      <img src="../SRC/logo.png" id="imagenLogoUsb" alt="Smiley face" height="70" width="120">
                    </div>

                </div>
                 
              </div>
              <div class="modal-body">
              
              <!-- ############################################################################# -->

                <!-- Aqui va el FAQ -->
                  

                    <button class="botonFAQ" id="botonFAQ1">
                      <div id="distroBoton">
                        <div id="textoBoton">
                          ¿Que es UMap? 
                        </div>

                        <div id="iconBoton">
                          <span class="glyphicon glyphicon-chevron-down" class="iconFAQ" id="iconFAQ1"></span>
                        </div>
                      </div>
                      
                    </button>
                    
                    <br>

                    <div class="divOculta" id="divOculta1" style="display:none">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque dolorem quisquam consectetur
                      distinctio odit aperiam amet eos accusantium vitae culpa praesentium molestias reprehenderit 
                      error modi dicta, tempora voluptas rem. Nam.
                    </div>
                  
                    <br>
                  <!-- segunda pregunta -->
                 
                  <button class="botonFAQ" id="botonFAQ2">
                      <div id="distroBoton">
                        <div id="textoBoton">
                          ¿Debo estar todo el tiempo conectado a Internet? 
                        </div>

                        <div id="iconBoton">
                          <span class="glyphicon glyphicon-chevron-down" id="iconFAQ2" class="iconFAQ2"></span>
                        </div>
                      </div>
                      
                    </button>
                    
                    <br>

                    <div class="divOculta" id="divOculta2" style="display:none">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque dolorem quisquam consectetur
                      distinctio odit aperiam amet eos accusantium vitae culpa praesentium molestias reprehenderit 
                      error modi dicta, tempora voluptas rem. Nam.
                    </div>

              <!-- ############################################################################# -->

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            
          </div>
        </div>
        
      </div>


    <!-- ################################################################################### -->
    <!-- Aqui coge el modal de la lupa -->

    <div class="container">
        <div class="modal fade" id="modalLupa" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              
              <div class="modal-body" >

                <div id="ContentSearch">
              
                  <div id="divIcon"> <i class="fas fa-search" id="lupaIcon"></i> </div>
                  <div id="divInput"> 

                  <input id="searchBar" type="text" placeholder="Buscar..." onkeydown="onKeyDownHandler(event);">

<!-- ##################################################################################################### -->
<!-- Aqui cogo que tecla se presiono para hacer le query -->

                  <script>
                  
                    function onKeyDownHandler(event) {
                      
                      var codigo = event.which || event.keyCode;
                      console.log("Presionada: " + codigo);
                                                             
                      if(codigo === 13){
                        console.log("Tecla ENTER");
                      }

                      if(codigo >= 65 && codigo <= 90){
                        console.log(String.fromCharCode(codigo));
                      }
                      
                    }

                  </script>
                  
                  </div>

                      <div id="divButton"> 
                      <button type="button" class="close" data-dismiss="modal">&times;
                      </button> 
                      </div>

                  </div>
                                 
              </div>
              
            </div>
            
          </div>
        </div>
        
      </div>
      

    <script src="../JS/MapaUSBCali.js"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnWWTCb-7kLTWWQUTTp_MFwPKEDxiGwsQ&callback=initMap"></script>  

</body>
</html>